import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appInputFormat]'
})
export class InputFormatDirective {
  @Input('appInputFormat') format: String;

  constructor(private el: ElementRef) {}

  @HostListener('blur') onBlur () {
    const nativeEl = this.el.nativeElement;

    if (this.format === 'uppercase') {
      nativeEl.value = (<string>nativeEl.value).toUpperCase();
    }
  }
}
