import { InputFormatDirective } from './input-format.directive';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { TitreComponent } from '../components/titre/titre.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UserService } from '../services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { LoginComponent } from '../components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('InputFormatDirective', () => {
  let fixture: ComponentFixture<LoginComponent>;
  let component: LoginComponent;


  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent, InputFormatDirective],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [UserService],
      imports: [
        HttpClientModule,
        FormsModule,
        RouterTestingModule
      ]
    });

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
  });

  it('should create TitreComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should have an input wiht the directive ', () => {
    const debugEl = fixture.debugElement.query(By.directive(InputFormatDirective));
    expect(debugEl).not.toBeNull();
  });

  it('shoud set value input to upperCase after blur', () => {
    const text = 'othmane';
    const debugEl = fixture.debugElement.query(By.directive(InputFormatDirective));
    const el: HTMLInputElement = debugEl.nativeElement;

    fixture.detectChanges();
    el.value = text;
    el.dispatchEvent(new Event('blur'));

    expect(el.value).toBe(text.toUpperCase());
  });

});
