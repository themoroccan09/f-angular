import { Component, OnInit } from '@angular/core';
import { select, NgRedux } from '@angular-redux/store';
import { IAppState } from 'src/app/store/store';
import { REMOVE_ALL_TODOS } from 'src/app/store/todos/store';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @select((s:IAppState) => s.todoing.items.length) count$;
  @select((s:IAppState) => s.todoing.lastUpdate) lastUpdate$;

  constructor(private ngRedux: NgRedux<IAppState>) {
    this.ngRedux.subscribe(() => {
      console.log(this.ngRedux.getState());
      
    })
   }

  ngOnInit() {
  }

  removeAll() {
    this.ngRedux.dispatch({type: REMOVE_ALL_TODOS})
  }

}
