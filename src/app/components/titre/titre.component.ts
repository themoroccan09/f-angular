import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-titre',
  templateUrl: './titre.component.html',
  styleUrls: ['./titre.component.css']
})
export class TitreComponent implements OnInit {
  isAuth: Boolean = false;
  titre: String = '16. Redux';

  constructor(private userService: UserService) { }

  ngOnInit() {}

}
