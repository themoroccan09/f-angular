import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidData = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit() {
  }

  login (form: NgForm) {
    this.authService.login(form.value).subscribe((result) => {
      if (result) {
        const redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');

        this.router.navigate([redirectUrl || 'home']);
      } else {
        this.invalidData = true;
      }
    });
  }

}
