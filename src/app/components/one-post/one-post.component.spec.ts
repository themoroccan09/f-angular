import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OnePostComponent } from './one-post.component';
import { Observable, empty, Subject } from 'rxjs';
import { ParamMap, Router, ActivatedRoute, convertToParamMap } from '@angular/router';


class RouterStub {
  navigate() { }
}
class ActivatedRouteStub {
  private subject: Subject<ParamMap> = new Subject();

  get paramMap() {
    return this.subject.asObservable();
  }
  push(val: number) {
    const param = convertToParamMap({ id: val });
    this.subject.next(param);
  }

}

describe('OnePostComponent', () => {
  let component: OnePostComponent;
  let fixture: ComponentFixture<OnePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OnePostComponent],
      providers: [
        { provide: Router, useClass: RouterStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate the user to posts when he clicks subimt : ', () => {
    const router = TestBed.get(Router);
    const spy = spyOn(router, 'navigate');

    component.onSubmit();

    expect(spy).toHaveBeenCalledWith(['posts'], { queryParams: { page: 1, order: 'newest' } });
  });

  it('should get the id from the route ', () => {
    const route = TestBed.get(ActivatedRoute);

    route.push(0);

    expect(component.id).toBe(0);
  });

});
