import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodosComponent } from './todos.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TodosService, Todo } from 'src/app/services/todos.service';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;
  const todos: Todo[] = [
    { id: 1, title: 'Go to the gym' },
    { id: 2, title: 'Pay Electricity' }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TodosComponent],
      imports: [BrowserAnimationsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create Todos component', () => {
    expect(component).toBeTruthy();
  });

  it('should get todos from service', async () => {
    component.ngOnInit();

    fixture.whenStable().then(() => {
      expect(component.todos.length).toBe(todos.length);
    });
  });

  it('should call add Method', () => {
    const spy = spyOn(component, 'add').and.callFake(() => { });

    component.add({} as HTMLInputElement);

    expect(spy).toHaveBeenCalled();
  });

  it('should add a todo', async () => {
    fixture.whenStable().then(() => {
      const todo: Todo = { id: 0, title: 'Test' };

      component.add({ value: todo.title } as HTMLInputElement);
      console.log(component.todos);

      const index = component.todos.findIndex(t => t.title == todo.title);

      expect(index).not.toBe(-1);
    });
  });

  it('should throw an error if the input value is empty or undefined', () => {
    const error = 'Error 1578';
    const service = new TodosService();
    const comp = new TodosComponent(service);
    const spy = spyOn(service, 'add').and.throwError(error);

    comp.add({} as HTMLInputElement);

    expect(comp.message as string).toBe(error);
  });

  it('should delete if the user config it', async () => {
    fixture.whenStable().then(() => {
      const todosCpt = component.todos.length;
      spyOn(window, 'confirm').and.returnValue(true);

      component.remove({ id: 1 });

      expect(component.todos.length).toBe(todosCpt - 1);
    });
  });

  it('should not delete todo if user cancels', () => {
    const cpt = component.todos.length;
    spyOn(window, 'confirm').and.returnValue(false);

    component.remove({ id: 1 });

    expect(component.todos.length).toBe(cpt);
  });

  xit('should get dependencies : ', () => {
    const service = TestBed.get(TodosService);
    const ser = fixture.debugElement.injector.get(TodosService);
  });
});
