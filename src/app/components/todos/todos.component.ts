import { Component, OnInit, OnDestroy } from '@angular/core';
import { transition, trigger, animate, style, state, useAnimation, query, animateChild, group, stagger } from '@angular/animations';
import { fade, slide, bounceOutLeft, bounceOutLeftAnimation, slideDownAnimation } from 'src/app/commons/animations';
import { TodosService, Todo } from 'src/app/services/todos.service';
import { Subscription, Observable } from 'rxjs';
import { select } from '@angular-redux/store';
import { IAppState } from 'src/app/store/store';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
  animations: [
    slide,
    fade,
    trigger('todosAnimation', [
      transition(':enter', [
        group([
          query('h3', slideDownAnimation),
          query('.list-group-item', [
            stagger(200, animateChild()
            )], { optional: true })
        ])
      ])
    ]),
    trigger('todoCostumAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate(1000)]),
      transition(':leave', [
        style({ backgroundColor: 'red' }),
        animate(1000),
        useAnimation(bounceOutLeftAnimation, {
          params: {
            duration: '.8s',
            startOpacity: .8,
            endOpacity: 0
          }
        })
      ])
    ])
  ]
})
export class TodosComponent {
  @select((s: IAppState) => s.todoing.items) todos$;
  message: string;
  constructor(private todosService: TodosService) {}


  add(input: HTMLInputElement) {
    try {
      this.todosService.add({title: input.value });
      input.value = '';
    } catch (e) {
      this.message = e.message;
    }
  }
  update(todo: Todo, input: HTMLInputElement) {
    todo.title = input.value;
    this.todosService.update(todo);
    input.blur();
  }
  remove (todo: Todo) {
    this.todosService.remove(todo.uid);
  }

  getTodo (uid: string) {
    const index = this.todos$.findIndex(item => item.uid == uid);
    return this.todos$[index];
  }
  trackById(index, todo) {
    return todo ? todo.uid : undefined; 
  }
  /*  remove (todo) {
      if (confirm('Are you sure that you would to delete this todo ?!')) {
        this.todosService.remove(todo.id);
      }
    } */

}
