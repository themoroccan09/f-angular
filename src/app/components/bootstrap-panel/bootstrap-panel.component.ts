import {Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bootstrap-panel',
  templateUrl: './bootstrap-panel.component.html',
  styles: [`
			span {
				color: red;
				background-color: gray; // This style will not be apply , because we can't use styles and styleUrls in the same time
			}
		`],
  styleUrls: ['./bootstrap-panel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BootstrapPanelComponent {
  @Output('updated') updated = new EventEmitter();

  onClick () {
    this.updated.emit({title: 'Object sent by bootstapPanel'});
  }


}
