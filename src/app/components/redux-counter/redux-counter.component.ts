import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from 'src/app/store/store';
import { INCREMENT } from 'src/app/store/messages/store';

@Component({
  selector: 'app-redux-counter',
  templateUrl: './redux-counter.component.html',
  styleUrls: ['./redux-counter.component.css']
})
export class ReduxCounterComponent implements OnInit {
  @select('counter') counter; // select property called counter into the store  
  

  constructor(private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() {
  }

  increment () {
    this.ngRedux.dispatch({type: INCREMENT })
  }

}
