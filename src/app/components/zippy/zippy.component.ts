import { Component, OnInit, Input, Output } from '@angular/core';
import { expandCollapsed } from './zippy.animation';

@Component({
  selector: 'app-zippy',
  templateUrl: './zippy.component.html',
  styleUrls: ['./zippy.component.css'],
  animations: [ expandCollapsed ]
})
export class ZippyComponent implements OnInit {
  private _isExpanded = false;

  constructor() { }

  ngOnInit() {
  }

  toggleContent () {
    this._isExpanded = !this._isExpanded;
  }

  isExpanded (): boolean {
    return this._isExpanded;
  }
}
