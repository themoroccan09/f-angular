import { transition, trigger, state, style, animate } from '@angular/animations';


export const expandCollapsed = trigger('expandZippy', [
    state('collapsed',
      style({
        height: 0,
        paddingTop: 0,
        paddingBottom: 0,
        opacity: 0
      })),
    state('expanded',
      style({
        height: '*',
        padding: '*',
        overflow: '*', // we can delete all this block because it is the defaut state done
        opacity: 1
      })),
    transition('collapsed => expanded', [
      animate('300ms ease-out',
        style({
          height: '*',
          paddingTop: '*',
          paddingBottom: '*'
        })
      ),
      animate('1s', style({opacity: 1}))
    ]),
    transition('expanded => collapsed', [animate('300ms ease-in')])
  ]);
