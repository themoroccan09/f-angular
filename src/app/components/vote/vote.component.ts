import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from 'src/app/store/store';
import { INCREMENT, DECREMENT } from 'src/app/store/messages/store';

@Component({
	selector: 'app-vote',
	templateUrl: './vote.component.html',
	styleUrls: ['./vote.component.css']
})

export class VoteComponent implements OnInit {
	@select((s: IAppState) => s.messaging.counter) vote$;

	constructor (private ngRedux: NgRedux<IAppState>) {
	}

	ngOnInit() {
	}

	like() { this.ngRedux.dispatch({type: INCREMENT}) }
	dislike() { this.ngRedux.dispatch({type: DECREMENT}) }

}
