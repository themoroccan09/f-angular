import { TestBed, ComponentFixture } from '@angular/core/testing';
import { VoteComponent } from './vote.component';
import { By } from '@angular/platform-browser';


describe('Vote Component Tests : ', () => {
	let fixture: ComponentFixture<VoteComponent>;
	let component: VoteComponent;

	beforeEach(() => {
		// Configure Module :
		TestBed.configureTestingModule({
			declarations: [VoteComponent]
		});

		// Create Fixture
		fixture = TestBed.createComponent(VoteComponent);
		component = fixture.componentInstance;

		fixture.detectChanges(); // to call ngOnInit
	});


	it('should render the total vote', () => {
		component.vote = 20;
		fixture.detectChanges();

		const de = fixture.debugElement.query(By.css('span'));
		const el: HTMLElement = de.nativeElement;

		expect(el.innerText).toContain('20');

	});

	it('should have class underline', () => {
		component.myVote = true;
		fixture.detectChanges();

		const de = fixture.debugElement.query(By.css('span'));

		expect(de.classes['underline']).toBeTruthy();

	});

	it('should upVote when the user click into the button', () => {
		const de = fixture.debugElement.query(By.css('button'));
		de.triggerEventHandler('click', null);

		expect(component.vote).toBe(1);
	});


});
