import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  navBarType: string;
  constructor(
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.navBarType = environment.navColor;
  }

  onLogout () {
    this.authService.logout();
    this.router.navigate(['home']);
  }

  isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }

  isAdmin (): boolean {
    const user = this.authService.currentUser;
    return (user && user.admin);
  }

}
