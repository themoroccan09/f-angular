import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsComponent } from './posts.component';
import { RouterModule, ActivatedRoute, ParamMap, convertToParamMap, Params } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { HttpClientModule, HttpParams } from '@angular/common/http';
import { Observable, of, from, empty } from 'rxjs';

xdescribe('PostsComponent', () => {
  let component: PostsComponent;
  let fixture: ComponentFixture<PostsComponent>;
  let postService: PostService;
  const posts = [{id: 0, title: 'Post 1'}, {id: 1 , title: 'Post 2'}];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsComponent ],
      providers: [PostService],
      imports: [RouterModule, HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  xit('should call getAll from the service', () => {
    // Arrange
    const spy = spyOn(postService, 'getAll').and.callFake(() => {
      return empty();
    });

    // Act
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  xit('should call method getAll from the service', () => {
  });

  xit('should call method getAll from the service', () => {
  });


});
