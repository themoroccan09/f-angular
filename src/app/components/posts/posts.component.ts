import { Component, OnInit } from '@angular/core';
import {PostService} from '../../services/post.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {combineLatest} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts;
  constructor(
    private service: PostService
  ) { }

  ngOnInit() {
    this.service.getAll().subscribe(posts => this.posts = posts);
  }

}
