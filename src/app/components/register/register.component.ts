import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MydialogComponent } from '../mydialog/mydialog.component';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from 'src/app/store/store';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  spinnerValue: number = 0;
  intervalSpinner: any;
  mydialog: MatDialogRef<MydialogComponent>;
  
  constructor(
    private dialog: MatDialog,
    private ngRedux: NgRedux<IAppState>,
    ) { }

  ngOnInit() {
  }

  startSpinner () {
    this.intervalSpinner = setInterval(() => {
      if(this.spinnerValue != 100) {
        this.spinnerValue++;
      } else {
        this.spinnerValue = 0;
        clearInterval(this.intervalSpinner);
      }
    }, 100)
  }

  openDialog () {
    this.mydialog = this.dialog.open(MydialogComponent, {
      data: {
        firstname: 'Othmane'
      }
    });
    this.mydialog.afterClosed().subscribe(val => console.log(val))
  }

}
