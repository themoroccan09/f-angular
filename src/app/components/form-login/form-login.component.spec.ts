import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormLoginComponent } from './form-login.component';
import { FormBuilder, FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';

describe('FormLoginComponent', () => {
  let component: FormLoginComponent;
  let fixture: ComponentFixture<FormLoginComponent>;
  let contact: FormGroup;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormLoginComponent ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormLoginComponent);
    component = fixture.componentInstance;
    contact = component.form.get('contact') as FormGroup;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sould have email,phone controls ', () => {
    expect(contact.get('email')).toBeTruthy();
    expect(contact.get('phone')).toBeTruthy();
  });

  it('email should be required', () => {
    contact.get('email').setValue('');
    expect(contact.get('email').valid).toBeFalsy();

    contact.get('email').setValue('some email');
    expect(contact.get('email').valid).toBeTruthy();
  });

});
