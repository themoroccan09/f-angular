import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MyValidatorsValidators} from '../../commons/MyValidators.validators';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css']
})
export class FormLoginComponent {
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      name: [],
      contact: fb.group({
        email: ['', Validators.required], // by default is a formControl
        phone: []
      }),
      topics: fb.array([])
    });
  }

  get topics (): FormArray {
    return this.form.get('topics') as FormArray;
  }
}
