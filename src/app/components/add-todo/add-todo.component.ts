import { Component, OnInit } from '@angular/core';
import { Todo, TodosService } from 'src/app/services/todos.service';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from 'src/app/store/store';
import { REMOVE_TODO, ADD_TODO } from 'src/app/store/todos/store';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {
  @select((s: IAppState) => s.todoing.items) todos$;
  
  constructor(
    private ngRedux: NgRedux<IAppState>,
    private todoService: TodosService    
    ) { 
  }

  ngOnInit() {
  }

  removeTodo(todo: Todo) {
    this.ngRedux.dispatch({type: REMOVE_TODO, todo })
  }
  addTodo (input: HTMLInputElement) {
    this.ngRedux.dispatch({type: ADD_TODO, todo: {title: input.value}})
    input.value = '';
  }

}
