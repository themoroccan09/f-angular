import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name : 'paragraphe'})
export class ParagrahePipe implements PipeTransform {
  transform(text: string, nbrCaracters?: number):  any {
    const n = (nbrCaracters) ? nbrCaracters : 50;
    if (!text) {
      return null;
    }

    return text.substr(0, n) + '...';
  }
}

