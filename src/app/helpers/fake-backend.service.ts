import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HTTP_INTERCEPTORS, HttpEvent, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { timeout, mergeMap } from 'rxjs/operators';
import { IfStmt } from '@angular/compiler';


@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  // tokenNotAdmin = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1vaGltaSBPdGhtYW5lIiwiYWRtaW4iOmZhbHNlLCJpYXQiOjE1MTYyMzkwMjJ9.5bjl-qTuEv4E3gLiOZiWtC4UNdAi5s2GxGWzF_FWOBQ';
  // tokenAdmin = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1vaGltaSBPdGhtYW5lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.QaAc_KnTAFVPuNU_tsIDfzpjQYeMnKDVmN1jsjmhofQ';
  token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1vaGltaSBPdGhtYW5lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.QaAc_KnTAFVPuNU_tsIDfzpjQYeMnKDVmN1jsjmhofQ';
  posts = [{id: 1, title: 'My local post 1'}, {id: 2, title: 'My local post 2'}];

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return of(null).pipe(
      mergeMap((val) => {
        // get Token
        if (req.url.endsWith('auth/login') && req.method == 'POST') {
          const user = JSON.parse(req.body);
          if (user.email === 'mohimi.othmane@gmail.com' && user.password === '1234') {
            return of(new HttpResponse({status: 200, body: JSON.stringify({token: this.token})}));
          } else {
            return of(new HttpResponse({status: 401, statusText: 'Unathorized'}));
          }
        }

        // get posts
        if (req.url.endsWith('posts') && req.method == 'GET') {
          const headers: HttpHeaders = req.headers;
          const accessToken = headers.get('Authorization');

          if (accessToken && accessToken == this.token) {
            return of(new HttpResponse({status: 200, body: this.posts}));
          } else if (!accessToken) {
            return of(new HttpResponse({status: 401, statusText: 'Unautorized'}));
          }

          return of(new HttpResponse({status: 403, statusText: 'Forbidden'}));
        }

        return next.handle(req);
      })
    );
  }

  constructor() { }
}


export let fackBackendService = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
