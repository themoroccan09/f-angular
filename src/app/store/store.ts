import { ITodosState, INIT_TODO_STATE, TodoReducer } from "./todos/store";
import { IMessaegingState, INIT_MESSAGING_STATE, MessagingReducer } from "./messages/store";
import { combineReducers } from 'redux'

export interface IAppState {
  todoing: ITodosState,
  messaging: IMessaegingState
}



export const INITIAL_APP_STATE: IAppState = { 
  todoing: INIT_TODO_STATE,
  messaging: INIT_MESSAGING_STATE
}


export const appReducer = combineReducers({
  todoing: TodoReducer,
  messaging: MessagingReducer
})