import { tassign } from "tassign";
import { Todo } from "src/app/services/todos.service";

// ACTIONS --------------------------------------
export const FETCH_TODOS = 'FETCH_TODOS';
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_ALL_TODOS = 'REMOVE_ALL_TODOS';
export const REMOVE_TODO = 'REMOVE_TODO';


// REDUCER ------------------------------------------
export function TodoReducer (state: ITodosState = INIT_TODO_STATE, action): ITodosState{
  switch(action.type) {
    case FETCH_TODOS:       return fetchTodos(state, action);
    case ADD_TODO:          return addTodo(state, action);
    case REMOVE_TODO:       return removeTodo(state, action);
    case REMOVE_ALL_TODOS:  return removeAllTodo(state, action);
  }
  return INIT_TODO_STATE;
}

// FUNCTIONS -------------------------------------

function fetchTodos (state: ITodosState, action) {
  console.log('Fetch data', action.todos);
  return tassign(state, {items: action.todos, lastUpdate: new Date()})
}
function addTodo (state: ITodosState, action) {
  return tassign(state, {items: state.items.concat(action.todo), lastUpdate: new Date()})
}
function removeTodo(state: ITodosState, action) {
  const items = state.items;
  return tassign(state,{items: items.filter((t: Todo) => t.uid != action.uid), lastUpdate: new Date()})
}
function removeAllTodo(state: ITodosState, action) {
  return tassign(state, {items: [], lastUpdate: new Date()})
}

// STATE INTERFACE AND INIT STATE--------------------------------------
export const INIT_TODO_STATE: ITodosState = 
  {
    items: [],
    lastUpdate: null
  }

export interface ITodosState {
  items: Todo [],
  lastUpdate: Date
}