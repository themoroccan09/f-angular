import { tassign } from "tassign";

// ACTIONS --------------------------------------
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

// REDUCER ------------------------------------------
export function MessagingReducer (state: IMessaegingState = INIT_MESSAGING_STATE, action): IMessaegingState  {
  switch(action.type) {
    case INCREMENT: return increment(state, action);
    case DECREMENT: return decrement(state, action);
  }
  return INIT_MESSAGING_STATE;
}

// FUNCTIONS -------------------------------------
function increment(state: IMessaegingState, action) {
  return  tassign(state, {counter: state.counter + 1})
}
function decrement(state: IMessaegingState, action) {
  return  tassign(state, {counter: state.counter - 1})
}
// STATE INTERFACE AND INIT STATE--------------------------------------
export interface IMessaegingState {
  counter: number
}
export const INIT_MESSAGING_STATE = {
  counter: 0
}

