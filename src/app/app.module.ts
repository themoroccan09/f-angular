import { HttpClientModule } from '@angular/common/http';
import { NgModule,isDevMode } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from 'src/environments/environment';
import { AdminGuard } from './admin-guard.service';
import { AppComponent } from './app.component';
import { routes } from './app.routes';
import { BodyComponent } from './components/body/body.component';
import { BootstrapPanelComponent } from './components/bootstrap-panel/bootstrap-panel.component';
import { FormLoginComponent } from './components/form-login/form-login.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { MydialogComponent } from './components/mydialog/mydialog.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NoAccessComponent } from './components/no-access/no-access.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { OnePostComponent } from './components/one-post/one-post.component';
import { PostsComponent } from './components/posts/posts.component';
import { RegisterComponent } from './components/register/register.component';
import { TitreComponent } from './components/titre/titre.component';
import { TodosComponent } from './components/todos/todos.component';
import { VoteComponent } from './components/vote/vote.component';
import { ZippyComponent } from './components/zippy/zippy.component';
import { InputFormatDirective } from './directives/input-format.directive';
import { fackBackendService } from './helpers/fake-backend.service';
import { ParagrahePipe } from './pipes/paragrahe.pipe';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { MaterialComponentsModule } from './material-components.module';
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';
import { ReduxCounterComponent } from './components/redux-counter/redux-counter.component';
import { fromJS,Map } from 'immutable';
import { AddTodoComponent } from './components/add-todo/add-todo.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TodosService } from './services/todos.service';
import { IAppState, appReducer, INITIAL_APP_STATE } from './store/store';


@NgModule({
  declarations: [
    AppComponent,
    TitreComponent,
    BodyComponent,
    ParagrahePipe,
    BootstrapPanelComponent,
    InputFormatDirective,
    FormLoginComponent,
    NavbarComponent,
    HomeComponent,
    PostsComponent,
    NotFoundComponent,
    OnePostComponent,
    LoginComponent,
    NoAccessComponent,
    TodosComponent,
    ZippyComponent,
    VoteComponent,
    RegisterComponent,
    MydialogComponent,
    ReduxCounterComponent,
    AddTodoComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    MaterialComponentsModule,
    NgReduxModule
  ],
  entryComponents: [ MydialogComponent ],
  providers: [
    UserService,
    // { provide: ErrorHandler, useClass: AppErrorHandler },
    AuthService,
    AdminGuard,
    fackBackendService,
    TodosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor (
    private ngRedux: NgRedux<IAppState>,
    private devTools: DevToolsExtension
    ) 
  {
    let enhancer = isDevMode ? [this.devTools.enhancer()]: []
    this.ngRedux.configureStore(appReducer, INITIAL_APP_STATE, [], enhancer);
  }
}
