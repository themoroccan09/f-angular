import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Subscription, Subject, Observable } from 'rxjs';
import { map, subscribeOn } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../store/store';
import { FETCH_TODOS, ADD_TODO, REMOVE_TODO } from '../store/todos/store';

@Injectable()
export class TodosService { 
  private subject: Subject<Todo[]> = new Subject();
  public todos: Observable<Todo[]> = this.subject.asObservable();

  constructor(
    private ngFDB: AngularFireDatabase,
    private ngRedux: NgRedux<IAppState>
  ) {

    this.ngFDB
      .list('/todos')
      .snapshotChanges()
      .pipe(
        map((observer) => {
          return observer.map(item => {
            return {
              uid: item.key,
              title: (item.payload.val() as Todo).title
            } as Todo
          })
        })
      ).subscribe((todos: Todo[]) => {
        this.subject.next(todos) // Page Todos
        this.ngRedux.dispatch({ type: FETCH_TODOS, todos: todos })
      })
  }
  add(todo: Todo) {
    todo.uid = Date.now().toString();
    this.ngRedux.dispatch({type: ADD_TODO, todo})
    
    this.ngFDB.list('/todos')
      .push(todo)
      .then(val => todo.uid = val.key)
  }
  update(todo: Todo) {
    this.ngFDB.object('/todos/' + todo.uid).update({ title: todo.title })
  }
  remove(uid: string) {
    this.ngRedux.dispatch({type: REMOVE_TODO, uid});
    this.ngFDB.object('/todos/' + uid).remove()
  }
}


export interface Todo {
  uid?: string;
  title: string;
}
