import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { pipe, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = 'http://localhost:4200';
  jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient) {
  }

  login (user): Observable<boolean> {
    return this.http.post(this.url + '/auth/login', JSON.stringify(user))
    .pipe(
      map((result: any) => {
        result = JSON.parse(result);
        if (result && result.token) {
          localStorage.setItem('token', result.token);
          return true;
        }
        return null;
      })
    );
  }
  logout (): void {
    localStorage.removeItem('token');
  }
  isLoggedIn (): boolean {
    return !(localStorage.getItem('token') == null);
  }
  get currentUser () {
    if (this.isLoggedIn()) {
      return this.jwtHelper.decodeToken(localStorage.getItem('token'));
    }
    return null;
  }

}
