import {Response, Headers} from '@angular/http';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {BadRequestError} from '../commons/bad-request-error';
import {NotFoundError} from '../commons/not-found.error';
import {AppError} from '../commons/app.error';
import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class DataService {

  constructor(private url: string, private http: HttpClient) { }
  getAll () {
    return this.http.get(this.url, {headers: new HttpHeaders({Authorization: localStorage.getItem('token')})})
      .pipe(
        catchError(this.handleError)
      );
  }
  create (resource) {
    return this.http.post(this.url, resource).pipe(
      map((res: Response) => res.json()),
      catchError(this.handleError)
    );
  }
  update (resource) {
    return this.http.put(this.url + '/' + resource.id, resource).pipe(
      map((res: Response) => res.json()),
      catchError(this.handleError)
    );
  }
  delete (id: number) {
    return this.http.delete(this.url + '/' + id).pipe(
      map((res: Response) => res.json()),
      catchError(this.handleError)
    );
  }

  private handleError (error: Response) {
    if (error.status === 400) {
      return throwError(new BadRequestError(error));
    }
    if (error.status === 404) {
      return throwError(new NotFoundError(error));
    }

    return throwError(new AppError(error));
  }
}
