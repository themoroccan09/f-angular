import { trigger, state, style, animate, transition, keyframes, animation, useAnimation} from '@angular/animations';

// Animations

export let bounceOutLeftAnimation = animation(
    animate('{{ duration }}', keyframes([
        style({offset: 0.3, transform: 'translateX(50px)', opacity: '{{ startOpacity }}'}),
        style({offset: 1, transform: 'translateX(-100%)', opacity: '{{ endOpacity }}'})
    ]))
);

export const slideDownAnimation = animation([
    style({transform: 'translateY(-20px)'}),
    animate(1000)
]);

// Triggers
export let fade = trigger('fade', [
    state('void', style({opacity: 0})),
    transition('void <=> *', animate(1000))
  ]);

export let slide = trigger('slide', [
    state('void', style({transform: 'translateX(-100%)'})),
    transition(':enter', animate('1s ease-out')),
    transition(':leave', animate('1s cubic-bezier(.11,.86,.63,.18)'))
]);

export let bounceOutLeft = trigger('bounceOutLeft', [
    transition(':leave', useAnimation(bounceOutLeftAnimation))
]);
