import {AbstractControl, ValidationErrors} from '@angular/forms';

export class MyValidatorsValidators {
  static canBeAlphabetic (input: AbstractControl): ValidationErrors | null {
    if (!(input.value as string).match('^[a-zA-Z]*$')) {
      return { canBeAlphabetic: true };
    }

    return null;
  }
  static shouldBeUnique (input: AbstractControl): Promise<ValidationErrors | null> {
    console.log('Should be Uniqu check');
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (input.value === 'Othmane') {
          console.log('should be alerted');
          resolve({shouldBeUnique: true});
        } else {
          resolve(null);
        }
      }, 2000);
    });
  }

}
