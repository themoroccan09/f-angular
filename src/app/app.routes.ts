import { Route } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { VoteComponent } from './components/vote/vote.component';
import { OnePostComponent } from './components/one-post/one-post.component';
import { AuthGuard } from './services/auth-guard.service';
import { AdminGuard } from './admin-guard.service';
import { PostsComponent } from './components/posts/posts.component';
import { LoginComponent } from './components/login/login.component';
import { NoAccessComponent } from './components/no-access/no-access.component';
import { TodosComponent } from './components/todos/todos.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RegisterComponent } from './components/register/register.component';



export const routes: Route[] = [
    {path: '', component: HomeComponent},
    {path: 'vote', component: VoteComponent},
    {path: 'posts', component: PostsComponent, canActivate: [AuthGuard, AdminGuard]},
    {path: 'posts/:id', component: OnePostComponent},
    {path: 'login', component: LoginComponent},
    {path: 'no-access', component: NoAccessComponent},
    {path: 'todos', component: TodosComponent, canActivate: [AuthGuard]},
    {path: 'register', component: RegisterComponent },
    {path: '**', component: NotFoundComponent},
  ];
