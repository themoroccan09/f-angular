import { routes } from './app.routes';
import { PostsComponent } from './components/posts/posts.component';
import { AuthGuard } from './services/auth-guard.service';
import { AdminGuard } from './admin-guard.service';

describe('routes', () => {
    it('should have posts route', () => {
			expect(routes).toContain({path: 'posts', component: PostsComponent, canActivate: [AuthGuard, AdminGuard]});
    });
});
