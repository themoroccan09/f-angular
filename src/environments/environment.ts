// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  navColor: 'default',
  firebase : {
    apiKey: "AIzaSyAz2_nTclna-7Y7jGG-1XRpt3e_kF0wBJQ",
    authDomain: "f-angular.firebaseapp.com",
    databaseURL: "https://f-angular.firebaseio.com",
    projectId: "f-angular",
    storageBucket: "f-angular.appspot.com",
    messagingSenderId: "783089948202"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
